package it.unibo.oop.lab.mvcio2;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import it.unibo.oop.lab.mvcio.Controller;

/**
 * A very simple program using a graphical interface.
 *
 */
public final class SimpleGUIWithFileChooser {

    /**
     *
     */
    public static final String TITLE = "Java application";
    private final JFrame frame = new JFrame(TITLE);

    /**
     *
     * @param ctrl the controller
     */
    public SimpleGUIWithFileChooser(final Controller ctrl) {
        //imposto cosa deve fare quando clicco la x
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //creo i vari oggetti che mi serviranno nell'interfaccia
        final JPanel panel = new JPanel();
        final JTextArea text = new JTextArea();
        final LayoutManager layout = new BorderLayout();
        //setto il layout
        panel.setLayout(layout);
        //specifico cosa deve fare il bottone save
        final JButton save = new JButton("SAVE"); //creo il bottone
        save.addActionListener(new ActionListener() { //setto il listener, che mi svelge il metodo saveContent di Controller
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                try {
                    ctrl.saveContent(text.getText());
                } catch (IOException e) {
                    JOptionPane.showMessageDialog(null, e.getMessage(), "An error has occured", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        //aggiungo le cose al pannello
        panel.add(text, BorderLayout.CENTER);
        panel.add(save, BorderLayout.SOUTH);

        JTextField textField = new JTextField(ctrl.filePath(ctrl.getFile())); //ora creo una jTextField, ovvero un area dove viene inserito del testo
        textField.setEditable(false); //la creo di modo che non sia modificabile

        JButton chooseFile = new JButton("Browse..."); //creo un nuovo bottone che sarà quello che andrà a cercare dove voglio salvare il file
        chooseFile.addActionListener(new ActionListener() { //aggiungo i passaggi di quello che voglio che faccia

            @Override
            public void actionPerformed(final ActionEvent e) {
                final JFileChooser fc = new JFileChooser("Choose where to save"); //creo un JFileChooser che mi fa decidere dove salvare il file
                fc.setSelectedFile(ctrl.getFile()); //nello specifico dove salvare il file corrente
                final int result = fc.showSaveDialog(frame); //salvo qui la cartella scelta
                switch (result) {
                case JFileChooser.APPROVE_OPTION: //se scelgo la cartella:
                    final File newDest = fc.getSelectedFile(); //imposto qui la nuova destinazione
                    ctrl.setDestination(newDest); //setto la nuova destinazione del file
                    textField.setText(newDest.getPath()); //e il percorso per arrivarci
                    break;
                case JFileChooser.CANCEL_OPTION: //se clicco cancella allora non fa niente (break)
                    break;
                default: //se per qualche ragione non succede nessuna delle due cose allora esce un messaggio di errore
                    JOptionPane.showMessageDialog(frame, result, "Meh!", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        JPanel upperPanel = new JPanel(); //fatto tutte le cose sopra creo un nuovo pannello
        upperPanel.setLayout(new BorderLayout()); //gli setto il nuovo layout

        upperPanel.add(textField, BorderLayout.CENTER); //aggiungo le cose che ho creato
        upperPanel.add(chooseFile, BorderLayout.LINE_END);
        panel.add(upperPanel, BorderLayout.NORTH); //e poi aggiungo tutto all'altro pannello

        frame.setContentPane(panel); //alla fine aggiungo il primo pannello (con dentro il secondo) nel frame

        //impostazioni dello schermo
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / 4, sh / 4);

        frame.setLocationByPlatform(true);
    }

    //funzione che mi setta la visibilità a true
    private void display() {
        frame.setVisible(true);
    }

    /**
     *
     * @param args input
     */
    //creo un nuovo oggetto simpleGUIwithFIleChooser (con un controller in input) e poi setto con display la visibilità di tutto quanto
    public static void main(final String... args) {
        final SimpleGUIWithFileChooser gui = new SimpleGUIWithFileChooser(new Controller());
        gui.display();
    }
}
