package it.unibo.oop.lab.mvcio;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

/**
 * class that does some simple operation on files, like setting the current file
 * get the file path, write a string in the current file.
 */
public class Controller {
    private static final String HOME = System.getProperty("user.home"); //serve per far funzionare l'applicazione in qualunque applicazione
    private static final String SEPARATOR = System.getProperty("file.separator");
    private static final String DEFAULT_FILE = "output.txt";

    private File file = new File(HOME + SEPARATOR + DEFAULT_FILE);

    /**
     *
     * @return current file
     */
    public File getFile() {
        return this.file;
    }

    /**
     *
     * @param file input
     * @return String of the path file
     */
    public String filePath(final File file) {
        return file.getPath();
    }

    /**
     *
     * @param text content to save
     * @throws IOException if something goes wrong
     */
    public void saveContent(final String text) throws IOException {
        try (PrintStream  out = new PrintStream(file)) {
            out.println(text);
        }
    }

    /**
     *
     * @param file set this file as new destination
     * @throw IllegalArgumentException if the new destination is not found
     */
    public void setDestination(final File file) {
        final File parent = file.getParentFile();
        if (parent.exists()) {
            this.file = file;
        } else {
            throw new IllegalArgumentException("Can not save in a non-existing folder");
        }
    }

    /**
     *
     * @param nameFile set the name of the new file
     */
    //setta la destionazione chiamando il metodo sopra, anche perchè io inizialmente
    //passo una stringa e in questo modo viene trasformata in file che viene passato al metodo sopra
    public void setDestination(final String nameFile) {
        setDestination(new File(nameFile));
    }

}
