package it.unibo.oop.lab.mvcio;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;


/**
 * A very simple program using a graphical interface.
 *
 */
public final class SimpleGUI {
    private static final String TITLE = "Controller Application"; //setto di sicuro il titolo
    private final JFrame frame = new JFrame(TITLE); //creo un nuovo frame e gli assegno il titolo sopra

    /**
     *
     * @param ctrl
     * ciao
     */
    //costruttore che imposta tutte le impostazioni della gui
    public SimpleGUI(final Controller ctrl) {
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //dico che se clicco x deve uscire
        final JPanel panel = new JPanel(); //creo gli strumenti che mi servono (pannello, textarea, layout)
        final JTextArea textArea = new JTextArea();
        final LayoutManager layout = new BorderLayout();
        panel.setLayout(layout); //setto il layout al pannello
        final JButton save = new JButton("SAVE"); //creo il bottone
        save.addActionListener(new ActionListener() { //setto il listener, che mi svelge il metodo saveContent di Controller
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                try {
                    ctrl.saveContent(textArea.getText());
                } catch (IOException e) {
                    JOptionPane.showMessageDialog(null, e.getMessage(), "An error has occured", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        panel.add(textArea, BorderLayout.CENTER); //aggiungo la text area in una certa posizione
        panel.add(save, BorderLayout.SOUTH); //aggiungo il bottone in un'altra
        frame.setContentPane(panel); //aggiungo tutto al frame

        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize(); //setto le dimensioni della finestra
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / 2, sh / 2);
        frame.setLocationByPlatform(true);
    }
    //metodo per settare visible l'interfaccia
    private void display() {
        frame.setVisible(true);
    }

    /**
     * main used to show the GUI.
     * @param args input
     */
    public static void main(final String... args) {
        final SimpleGUI gui = new SimpleGUI(new Controller());
        gui.display();
    }

}
