
package it.unibo.oop.lab.mvc;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Controller.
 * It implements all the method to control that Strings are corrected
 */
public class SimpleController implements Controller {

    private List<String> listaStringhe = new LinkedList<String>(); //creo una lista concatenata per contenere la lista di stringhe (così posso ottenere la storia)
    private String nextString;

    @Override
    public final void setNextStringToPrint(final String string) {
        this.nextString = Objects.requireNonNull(string, "Non puoi inserire un valore nullo"); //setto la prossima stringa di modo che sia obbligato a ricevere una stringa non nulla
    }

    @Override
    public final String getNextStringToPrint() {
        return this.nextString;
    }

    @Override
    public final List<String> getStringHistory() {
        return this.listaStringhe;
    }

    @Override
    public final void printCurrentString() {
        if (this.nextString == null) {
            throw new IllegalStateException("Non ci sono stringhe");
        }
        this.listaStringhe.add(this.nextString);
        System.out.println(this.nextString);
    }
}
