package it.unibo.oop.lab.mvc;

import java.util.List;

/**
 * A controller that prints strings and has memory of the strings it printed.
 */
public interface Controller {

    /**
     * 
     *  @param string it sets the next string to print
     */
    void setNextStringToPrint(String string);

    /**
     *
     * @return the next string to print
     */
    String getNextStringToPrint();

    /**
     * 
     * @return all the string that I have printed
     */
    List<String> getStringHistory();

    /**
     * it prints the current string.
     */
    void printCurrentString();
}
