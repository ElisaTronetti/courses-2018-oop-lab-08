package it.unibo.oop.lab.mvc;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * A very simple program using a graphical interface.
 * 
 * il funzionamento della gui è quello di stampare in standard output le stringhe che mi servono
 * mentre se richiedo la storia delle strighe viene stampato tutto nella textArea.
 * Lavora tutto con delle liste concatenate
 */

public final class SimpleGUI {

    private static final String TITLE = "String reader";
    private final JFrame frame = new JFrame(TITLE);
    private final Controller controller;

    /**
     * builds a new {@link SimpleGUI}.
     */
    public SimpleGUI() {
        this.controller = new SimpleController();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //creo e setto tutte le cose che mi servono
        final JPanel canvas = new JPanel();
        LayoutManager layout = new BorderLayout();
        canvas.setLayout(layout);

        JPanel southPanel = new JPanel();
        southPanel.setLayout(new BorderLayout());
        canvas.add(southPanel, BorderLayout.SOUTH);

        JTextField textField = new JTextField();
        canvas.add(textField, BorderLayout.NORTH);
        JTextArea textArea = new JTextArea();
        canvas.add(textArea, BorderLayout.CENTER);
        textArea.setEditable(false);
        JButton buttonPrint = new JButton("Print String");
        JButton buttonHistory = new JButton("Print String History");

        southPanel.add(buttonHistory, BorderLayout.EAST);
        southPanel.add(buttonPrint, BorderLayout.WEST);

        buttonPrint.addActionListener(new ActionListener() { //setto l'azione del bottone print
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                controller.setNextStringToPrint(textField.getText()); //prende il testo nell'area
                controller.printCurrentString(); //e lo stampa nella console
            }
        });

        buttonHistory.addActionListener(new ActionListener() { //setto l'azione del bottone per stampare tutta la storia delle strighe
            @Override
            public void actionPerformed(final ActionEvent e) {
                final StringBuilder text = new StringBuilder();
                final List<String> history = controller.getStringHistory(); //prende la storia delle stringhe
                for (final String print: history) { //per ogni stringa nella storia viene stampata e poi una new line
                    text.append(print);
                    text.append('\n');
                }
                if (!history.isEmpty()) { //se la storia non è vuota viene pulita
                    text.deleteCharAt(text.length() - 1);
                }
                textArea.setText(text.toString());
           }
        });

        frame.setContentPane(canvas);

        //impostazioni dello schermo
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / 2, sh / 2);

        frame.setLocationByPlatform(true);
    }

    private void display() {
        frame.setVisible(true);
    }

    /**
     * 
     * @param args main
     */
    public static void main(final String[] args) {
        SimpleGUI gui = new SimpleGUI();
        gui.display();
    }
}
