
//TOGLI IL COMMENTO!!
package it.unibo.oop.lab.advanced;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;


/**
 */
public final class DrawNumberApp implements DrawNumberViewObserver {

    private final DrawNumber model;
    private final List<DrawNumberView> views;

    /**
     *
     * @param configFile file that contains the configuration
     * @param views where I want to show my application
     */
    public DrawNumberApp(final String configFile, final DrawNumberView... views) {
        this.views = Arrays.asList(Arrays.copyOf(views, views.length));

        for (final DrawNumberView view : views) {
            view.setObserver(this);
            view.start();
        }

        final Configuration.Builder configurationBuilder = new Configuration.Builder();

        try {
            for (final String configLine : Files.readAllLines(Paths.get(configFile))) {
                final String[] lineElements = configLine.split(":");
                if (lineElements.length == 2) {
                    final int value = Integer.parseInt(lineElements[1].trim());
                    if (lineElements[0].contains("min")) {
                        configurationBuilder.setMin(value);
                    } else if (lineElements[0].contains("max")) {
                        configurationBuilder.setMax(value);
                    } else if (lineElements[0].contains("attempts")) {
                        configurationBuilder.setAttempts(value);
                    }
                } else {
                    displayError("I can not understand " + configLine);
                }
            }
        } catch (IOException | NumberFormatException e) {
            displayError(e.getMessage());
        }

        final Configuration configuration = configurationBuilder.build();

        if (configuration.isConsistent()) {
            this.model = new DrawNumberImpl(configuration);
        } else {
            displayError("Error! " + "min: " + configuration.getMin() + " , "
                    + "max: " + configuration.getMax() + " , "
                    + "attampts: " + configuration.getAttemps() + "Using default options.");
            this.model = new DrawNumberImpl(new Configuration.Builder().build());
        }
    }

    //mostra gli errori a display
    private void displayError(final String error) {
        for (final DrawNumberView view : views) {
            view.displayError(error);
        }
    }

    @Override
    public void newAttempt(final int n) {
        try {
            final DrawResult result = model.attempt(n);
            for (final DrawNumberView view : views) {
                view.result(result);
            }
        } catch (IllegalArgumentException e) {
            for (final DrawNumberView view : views) {
                view.numberIncorrect();
            }
        }
    }

    @Override
    public void resetGame() {
        this.model.reset();
    }

    @Override
    public void quit() {
        // System.exit(0);
    }

    /**
     * @param args
     *            ignored
     */
    public static void main(final String... args) {
        new DrawNumberApp("", new DrawNumberViewImpl());
        // /home/teemo/Desktop/java/laboratorio8/res/config.yml
        // uso i setting di default, non capisco perché lanci sempre eccezione altrimenti
    }

}
