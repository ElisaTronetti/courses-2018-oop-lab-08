
package it.unibo.oop.lab.advanced;

/**
 * configuration of the game.
 * all the options 
 * and the controls to verify that the option are possible
 *
 */
public final class Configuration {

    private final int max, min, attemps;
 
    Configuration(final int max, final int min, final int attemps) {
        this.max = max;
        this.min = min;
        this.attemps = attemps;
    }

    /**
     * 
     * @return max number of attempt allowed
     */
    public int getMax() {
        return this.max;
    }

    /**
     * 
     * @return minimum number of attempt allowed
     */
    public int getMin() {
        return this.min;
    }

    /**
     * 
     * @return current number of attempts
     */
    public int getAttemps() {
        return this.attemps;
    }

    /**
     * 
     * @return true if the options are corrected
     */
    public boolean isConsistent() {
        return this.attemps > 0 && min < max;
    }

     /**
     * 
     * Class that allowed to use the Builder once.
     *
     */
    public static class Builder {
        private static final int MIN = 0;
        private static final int MAX = 100;
        private static final int ATTEMPTS = 10;

        private int min = MIN;
        private int max = MAX;
        private int attempts = ATTEMPTS;
        private boolean consumed;

        /**
         * 
         * @param min set
         * @return the object
         */
        public Builder setMin(final int min) {
            this.min = min;
            return this;
        }

        /**
         * 
         * @param max set
         * @return the object
         */
        public Builder setMax(final int max) {
            this.max = max;
            return this;
        }

        /**
         * 
         * @param attempts set
         * @return the object
         */
        public Builder setAttempts(final int attempts) {
            this.attempts = attempts;
            return this;
        }

        /**
         * 
         * @return the configuration of the option
         *         it can not be used more than once
         */
        public final Configuration build() {
            if (consumed) {
                throw new IllegalStateException("The builder can only be used once");
            }
            consumed = true;
            return new Configuration(max, min, attempts);
        }
    }
}
